package com.spring.rest;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.rest.Employee;
import com.spring.rest.repository.EmployeeRepository;
@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeRepository employeeRepo;	
	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		return employeeRepo.findAll();
	}
	
	@GetMapping("/employees/{emp_id}")
	public Employee getOneEmployees(@PathVariable("emp_id") Long id) {
		return employeeRepo.getById(id);
		
			}
	
	@PostMapping("/employees")
	public Employee addEmployees(@RequestBody Employee requestEmployees) {
		return employeeRepo.save(requestEmployees);
		
	}
	@PutMapping("/employees/{emp_id}")
	public Employee updateEmployee(@PathVariable("emp_id")Long id, @RequestBody Employee newEmployeesValue)
	{
	Employee e= employeeRepo.getById(id) ;
	if(e!=null){
		e.setFirst_name(newEmployeesValue.getFirst_name());
		e.setLast_name(newEmployeesValue.getLast_name());
		e.setEmail(newEmployeesValue.getEmail());
		e.setPhoneNo(newEmployeesValue.getPhoneNo());
		e.setHire_date(newEmployeesValue.getHire_date());
		e.setJob_id(newEmployeesValue.getJob_id());
		e.setSalary(newEmployeesValue.getSalary());
		e.setManager_id(newEmployeesValue.getManager_id());
		
	}
	return employeeRepo.save(e);
	}
	
	@DeleteMapping("/employees/{emp_id}")
	public String deleteEmployees(@PathVariable("emp_id") Long id) {
		Employee  e= employeeRepo.getById(id);
		if(e!=null) {
			employeeRepo.delete(e);
			return "employee deleted";
		}
		else {
		
		return "Employees not available.";
		
	}
	}
}